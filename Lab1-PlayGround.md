# Lab 1: AWS Sandbox 

## Introduction

1) Go to [A Cloud Guru](acloud.guru) 	
2) Log in with your account.
    * If you did not set up your account before today, please message the organizer ASAP. We will do our best to help accommodate, but there is no guarantee.  
3) On the top navigation bar, click on “Cloud Playground.” 
4) Click on “AWS Sandbox.” 
    * If you have any questions on how these environments work, watch the “What are sandboxes video located on the top right corner.
    * Note: We are monitoring for any abuse of these environments. If abuse is detected, your session will be terminated, and you will no longer be able to participate in this workshop. 
5) Click on “Open Sandbox”
    * Note: You may want to copy the open sandbox link and paste it into an incognito window to ensure that previous sessions do not conflict with your current session. 


### Additional Resources

[AWS Free Tier](https://aws.amazon.com/free/)